<?php



interface FileToDownload
{
    public function __construct(string $name, string $path, string $contentType);

    /**
     * @return string
     */
    public function getName(): string;


    /**
     * @return string
     */
    public function getPath(): string;


    /**
     * @return string
     */
    public function getContentType(): string;

}