<?php


class File implements FileToDownload
{
    private $name;
    private $path;
    private $contentType;

    public function __construct(string $name, string $path, string $contentType)
    {
        $this->name = $name;
        $this->path = $path;
        $this->contentType = $contentType;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getPath(): string
    {
        return $this->path;
    }

    /**
     * @return string
     */
    public function getContentType(): string
    {
        return $this->contentType;
    }
}