<?php

use MicrosoftAzure\Storage\Blob\BlobRestProxy;
use MicrosoftAzure\Storage\Common\Exceptions\ServiceException;
use MicrosoftAzure\Storage\Blob\Models\SetBlobPropertiesOptions;


class AzureBlobFileHelper
{
    private $connectionString;
    private $container;
    private $blobClient;
    private $folder;

    public function __construct(string $connectionString, $container = '$web', $folder = '')
    {
        $this->connectionString = $connectionString;
        $this->container = $container;
        $this->blobClient = BlobRestProxy::createBlobService($this->connectionString);
        $this->folder = $folder;

        $this->createContainerIfNOtExists();
    }

    /**
     * upload file to Azure
     * $file param is from $_FILES array
     * @param FileToDownload $file
     * @return array|bool|string
     */
    public function upload(FileToDownload $file)
    {
        $errors = [];
        try {
            $opts = new SetBlobPropertiesOptions();
            $opts->setContentType($file->getContentType());

            try {
                $blobName = $this->generateBlobName($file->getName());

                $this->blobClient->createBlockBlob($this->container, $blobName, file_get_contents($file->getPath()));
                $this->blobClient->setBlobProperties($this->container, $blobName, $opts);

                $url = $this->blobClient->getBlobUrl($this->container, $blobName);
                return $url;
            } catch (ServiceException $e) {
                $code = $e->getCode();
                $error_message = $e->getMessage();
                $errors[] = $code . ": " . $error_message . PHP_EOL;
            }

        } catch (RuntimeException $e) {
            $code = $e->getCode();
            $error_message = $e->getMessage();
            $errors[] = $code . ": " . $error_message . PHP_EOL;
        }

        if ($errors) {
            return $errors;
        }

        return true;
    }

    public function delete($containerName, $filePath): void
    {
        $blobClient = BlobRestProxy::createBlobService($this->connectionString);
        $blobClient->deleteBlob($containerName, $filePath);
    }

    private function guid(): string
    {
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    /**
     * @param string $inputName
     * @return string
     */
    private function generateBlobName(string $inputName): string
    {
        return (!empty($this->folder) ? $this->folder : $this->guid()) . '/' . $inputName;
    }

    /**
     * returns all blobs in the container
     * @return \MicrosoftAzure\Storage\Blob\Models\ListBlobsResult
     */
    public function getBlobList()
    {
        return $this->blobClient->listBlobs($this->container);
    }

    /**
     * if we call a container that not exists error happens
     * to prevent this we create a container if it not exists
     */
    private function createContainerIfNOtExists(): void
    {
        try {
            $this->blobClient->getContainerAcl($this->container);
        } catch (RuntimeException $e) {
            $this->blobClient->createContainer($this->container);
        }
    }

}